import * as Http from 'http';
import Express from 'express';
import * as BodyParser from 'body-parser';
import * as Path from 'path';

const hostname = 'localhost';
const port = 3000;
const rootDir = Path.join(__dirname, '..');
const assets = Path.join(rootDir, 'client/assets');
const html = Path.join(rootDir, 'client/html');
const libs = Path.join(rootDir, '../node_modules');

const server = Express();

// server.use(Express.static(rootDir));
server.use(Express.static(assets));
server.use(Express.static(html));
server.use(Express.static(libs));

server.use(BodyParser.json());
server.use(BodyParser.urlencoded({
    extended: false
}));

const http = Http.Server(server);
const io = require('socket.io')(http);

io.on('connection', async (socket) => {
    console.log('User connected');

    socket.on('message', async (message) => {
        io.emit('message', message);
    });
});

http.listen(port, hostname, async () => {
    console.log(`Server running at http://${hostname}:${port}`);
    console.log(`Static files directory: ${html}`);
});